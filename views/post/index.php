<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Posts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Post', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'body:ntext',
            [
                'label'=>'Category',
                'value'=>'category.name', 
            ],
            [
                'label'=>'Status',
                'value'=>'user2.name', 
            ],
            [
                'label'=>'Author',
                'value'=>'user3.name', 
            ],
            
            'created_at',
            'updated_at',
            
            [
                'label'=>'Created By',
                'value'=>'user.name', 
            ],
            [
                'label'=>'Updated By',
                'value'=>'user1.name', 
            ],
          

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
<?php

    

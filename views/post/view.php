<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Post */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'body:ntext',
            [
                'label'=>'Category',
                'value'=>$model->category->name  
            ],
            [ 
                'label' => 'Author',
				'format' => 'html',
				'value' => Html::a($model->user3->name, 
					['user/view', 'id' => $model->user3->id]),  
            ],
            
            
            [
                'label'=>'Status',
                'value'=>$model->user2->name  
            ],
            'created_at',
            'updated_at',
            [
                'label'=>'Created By',
                'value'=>$model->user->name  
            ],

            [
                'label'=>'Updated By',
                'value'=>$model->user1->name  
            ],
          
        ],
    ]) ?>

</div>

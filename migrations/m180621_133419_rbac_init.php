<?php

use yii\db\Migration;

/**
 * Class m180621_133419_rbac_init
 */
class m180621_133419_rbac_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $auth = Yii::$app->authManager;
          $author = $auth->createRole('author');
          $auth->add($author);
    
          $admin = $auth->createRole('admin');
          $auth->add($admin);

          $editor = $auth->createRole('editor');
          $auth->add($editor);
                  
          $auth->addChild($admin, $editor);
          $auth->addChild($editor, $author);
    
          $manageUsers = $auth->createPermission('manageUsers');
          $auth->add($manageUsers);
    
          $createPosts = $auth->createPermission('createPosts');
          $auth->add($createPosts);    

          $updatePosts = $auth->createPermission('updatePosts');
          $auth->add($updatePosts);
           
          $deletePosts = $auth->createPermission('deletePosts');
          $auth->add($deletePosts);

          $publishPost = $auth->createPermission('publishPost');
                $auth->add($publishPost); 
                  
          $updateOwnPost = $auth->createPermission('updateOwnPost');
    
          $rule = new \app\rbac\AuthorRule;
          $auth->add($rule);
                  
          $updateOwnPost->ruleName = $rule->name;                
          $auth->add($updateOwnPost);                 
                                    
                  
          $auth->addChild($admin, $manageUsers);
          $auth->addChild($editor, $publishPost); 
          $auth->addChild($author, $createPosts);
          $auth->addChild($editor, $updatePosts);
          $auth->addChild($editor, $deletePosts);
          $auth->addChild($author, $updateOwnPost);
          $auth->addChild($updateOwnPost, $updatePosts);
         
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180621_133419_rbac_init cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180621_133419_rbac_init cannot be reverted.\n";

        return false;
    }
    */
}
